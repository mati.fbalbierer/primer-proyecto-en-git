import React from "react"
import  ReactDOM  from "react-dom"

class Script extends React.Component{
  render() {
    return (
      <div>
        <Reloj/>
        <CampoTexto />
      </div>
    )

  }
}


class CampoTexto extends React.Component{
  constructor(props){
    super(props)
    this.state={nombre: null}

  }
  handlerChange(e){
    this.setState({nombre: e.target.value})
  }
  render(){
    return(
      <div>
        <input placeholder="Tu nombre" type="text" onChange={this.handlerChange.bind(this)}/>
        <p>
          Hola, {this.state.nombre}
        </p>
      </div>
    )
  }
}

class Reloj extends React.Component{
  constructor(props){
    super(props) 
    this.state = {date: new Date()}

  }
  tick(){
    this.setState ({
      date: new Date()
    })
  }

  componentDidMount (){
    this.timerId = setInterval(this.tick.bind(this), 1000)
  }

  componentWillUnmount (){
    clearInterval(this.timerId)
  }
  render(){
    return (
      <div>
        <h1>
          Hello world
        </h1>
        <h2>
          it is {this.state.date.toLocaleTimeString()}
        </h2>
      </div>
    )
  }
}

ReactDOM.render(
  <Script/>,
  document.getElementById("root")
)